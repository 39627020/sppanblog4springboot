package net.sppan.blog.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import net.sppan.blog.entity.Blog;

public interface BlogDao extends Repository<Blog,Long> {

	 @Query("select a from Blog a where a.id = ?1") 
	 public Blog findById(Long id);
	 
	 @Transactional
	 @Modifying
	 @Query("update Blog a set views = views + :views where a.id = :id") 
	 public void incrView(@Param("views")int views,@Param("id")long id); 

}
