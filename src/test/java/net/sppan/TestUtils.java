package net.sppan;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.sppan.blog.utils.ExcelKit;

public class TestUtils {   
	@Test
	public void testReadExcel() {
		try {
			List<String[]> result = ExcelKit.read("D:/data/photo/201601四川电机职院1525人新生/201601四川电机职院1525人大专新生.xls", 0, 16);
			for(String[] data : result) {
				for(String cell :data) {
					System.out.print(cell + " ");
				}
				System.out.println("");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testWriteExcel() {
		try {
			List<String[]> rows = new ArrayList<String[]>();
			String[] row1 = {"512921197804280111","18628323995","江泽东"};
			String[] row2 = {"512921197804280121","18628323994","江东"};
			rows.add(row1);
			rows.add(row2);
			ExcelKit.write("d:/test.xlsx", rows);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
