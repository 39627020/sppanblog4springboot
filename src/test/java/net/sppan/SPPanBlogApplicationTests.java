package net.sppan;

import java.util.List;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import net.sppan.blog.AsyncTask;
import net.sppan.blog.SPPanBlogApplication;
import net.sppan.blog.dao.BlogDao;
import net.sppan.blog.entity.Blog;
import net.sppan.blog.service.BlogService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=SPPanBlogApplication.class)
public class SPPanBlogApplicationTests {

	@Resource
	private BlogService blogService;
	
	@Autowired BlogDao blogDao;
	
    @Autowired
    private AsyncTask asyncTask;
    
	@Test
	public void contextLoads() {
		List<Blog> list = blogService.findHotN(5);
		System.out.println(list);
	}
	
	@Test
	public void testBlogDao() {
		Blog blog = blogDao.findById(1L);
		System.out.println(blog.getViews());
	}
	
	@Test
	public void testModify() {
		blogDao.incrView(1, 1);
		Blog blog = blogDao.findById(1L);
		System.out.println(blog.getViews());
	}
	
	@Test
    public void testDealNoReturnTask(){
        asyncTask.dealNoReturnTask();
        try {
            System.out.println("begin to deal other Task!");
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
	
	@Test
    public void testDealHaveReturnTask() throws Exception {

        Future<String> future = asyncTask.dealHaveReturnTask();
        System.out.println("begin to deal other Task!");
        while (true) {
            if(future.isCancelled()){
            	System.out.println("deal async task is Cancelled");
                break;
            }
            if (future.isDone() ) {
            	System.out.println("deal async task is Done");
            	System.out.println("return result is " + future.get());
                break;
            }
            System.out.println("wait async task to end ...");
            Thread.sleep(1000);
        }
    }
}
